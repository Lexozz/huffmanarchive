﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using HoffmansArchivator.BinaryTree;
using HoffmansArchivator.InputOperations;

namespace HoffmansArchivator
{
    class Program
    {
        static void Main(string[] args)
        {
            //http://snipd.net/huffman-coding-in-c Huffman Tree code source
  

            FileOperations.FileInstance inputFile = new FileOperations.FileInstance("d:\\Program - ver.cs");
            FileOperations.FileInstance outputFile = new FileOperations.FileInstance("d:\\archivedFile.lexarch");

            string inputFileString = inputFile.ReadFileString();
            HuffmanTree huffmanTreeFile = new HuffmanTree();
            huffmanTreeFile.Build(inputFileString);
            BitArray encodedFile = huffmanTreeFile.EncodeString(inputFileString);

            Console.WriteLine("File writing is next");
            outputFile.WriteFileBytes(encodedFile);

            FileOperations.FileInstance decodedFile = new FileOperations.FileInstance("d:\\Program - ver_decoded.cs");
            byte[] outputFileString = outputFile.ReadFileBytes();
            string decoded = huffmanTreeFile.Decode(new BitArray(outputFileString));
            decodedFile.WriteFileString(decoded);

            Console.ReadLine();
        }
    }
}
