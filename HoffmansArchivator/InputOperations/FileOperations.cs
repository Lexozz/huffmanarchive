﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HoffmansArchivator.BinaryTree;

namespace HoffmansArchivator.InputOperations
{
    class FileOperations
    {
        public class FileInstance
        {
            public string Path { get; set; }

            public FileInstance(string pathToFile)
            {
                this.Path = pathToFile;
            }
            
            public string ReadFileString()
            {
                
                try
                {
                    using (StreamReader sr = new StreamReader(Path))
                    {
                        String line = sr.ReadToEnd();
                        Console.WriteLine("File text read successfully.");
                        return line;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception during file read occured: {0}", e);
                    return null;
                }
                
            }

            public byte[] ReadFileBytes()
            {

                try
                {
                    byte[] inputBytes = File.ReadAllBytes(Path);
                    Console.WriteLine("File bytes read successfully.");
                    return inputBytes;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception during file read occured: {0}", e);
                    return null;
                }

            }

            public void WriteFileString(string data)
            {
                try
                {
                    System.IO.StreamWriter file = new System.IO.StreamWriter(Path);
                    file.WriteLine(data);
                    file.Close();
                    Console.WriteLine("File saved successfully.");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception during file writing occured: {0}", e);
                }
            }

            public void WriteFileBytes(BitArray bits)
            {

                byte[] byteData = ToByteArray(bits);
                string _keyPath = Path + ".keys";
                try
                {
                    File.WriteAllBytes(_keyPath, byteData);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception during keys file writing occured: {0}", e);
                }
            }

            public static byte[] ToByteArray(BitArray bits)
            {
                int numBytes = bits.Count / 8;
                if (bits.Count % 8 != 0) numBytes++;

                byte[] bytes = new byte[numBytes];
                int byteIndex = 0, bitIndex = 0;

                for (int i = 0; i < bits.Count; i++)
                {
                    if (bits[i])
                        bytes[byteIndex] |= (byte)(1 << (7 - bitIndex));

                    bitIndex++;
                    if (bitIndex == 8)
                    {
                        bitIndex = 0;
                        byteIndex++;
                    }
                }

                return bytes;
            }

            public void WriteFileBinaryTree(byte[] bits)
            {
       
                string _keyPath = Path + ".keys";
                try
                {
                    File.WriteAllBytes(_keyPath, bits);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception during keys file writing occured: {0}", e);
                }
            }

        }
    }
}


/*  string input = "foo bar";
  HuffmanTree huffmanTree = new HuffmanTree();

  // Build the Huffman tree
  huffmanTree.Build(input);

  // EncodeString
  BitArray encoded = huffmanTree.EncodeString(input);

  Console.Write("Encoded: ");
  foreach (bool bit in encoded)
  {
      Console.Write((bit ? 1 : 0) + "");
  }
  Console.WriteLine();

  // Decode
  string decoded = huffmanTree.Decode(encoded);

  Console.WriteLine("Decoded: " + decoded);*/
