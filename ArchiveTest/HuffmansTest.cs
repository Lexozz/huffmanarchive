﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using HoffmansArchivator.BinaryTree;

namespace ArchiveTest
{
    public class HuffmansTest
    {

        [Test]
        public void HuffmanCodingTestSmallLetters()
        {
            string _input = "beep boop beer";
            string _output = null;
            HuffmanTree huffmanTree = new HuffmanTree();

            huffmanTree.Build(_input);

            BitArray encoded = huffmanTree.EncodeString(_input);

            string decoded = huffmanTree.Decode(encoded);
            Assert.AreEqual(_input, decoded);
        }

        [Test]
        public void HuffmanCodingTestCapitallLetters()
        {
            string _input = "Beep :Boop bEEr/";
            string _output = null;
            HuffmanTree huffmanTree = new HuffmanTree();

            huffmanTree.Build(_input);

            BitArray encoded = huffmanTree.EncodeString(_input);

            string decoded = huffmanTree.Decode(encoded);
            Assert.AreEqual(_input, decoded);
        }

        [Test]
        public void HuffmanCodingTestBinaryData()
        {
            string _input = "1512000000000000000000000000444444557744AF000011111245450000000001020304050102030405060102030405";
            string _output = null;
            HuffmanTree huffmanTree = new HuffmanTree();

            huffmanTree.Build(_input);

            BitArray encoded = huffmanTree.EncodeString(_input);

            string decoded = huffmanTree.Decode(encoded);
            Assert.AreEqual(_input, decoded);
        }


    }
}
